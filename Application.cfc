component {
    this.sessionManagement = true;

    this.datasource = "cf_bugtracker";
    this.ormEnabled = true;
    this.ormSettings = { logsql : true };
    this.invokeImplicitAccessor = true;

}