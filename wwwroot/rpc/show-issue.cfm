<cfif not IsDefined("session.auth.isLoggedIn")>
    <cflocation url="/" addtoken="false">
<cfelse>
    <cfset issuesService = new cfc.service.IssueService()>
    <cfset id = (isDefined("url.id") ? url.id : 0)>
    <cfset pageData = issuesService.showIssue(id)>
    <cfset issue = pageData.issue>
    <cfinclude template="/templates/header.template.cfm"/>
    <cfinclude template="/templates/show-issue.template.cfm"/>
    <cfinclude template="/templates/footer.template.cfm"/>
</cfif>
