<cfif not IsDefined("session.auth.isLoggedIn")>
    <cfinclude template="/templates/header.template.cfm"/>
    <cfinclude template="/templates/show-login-form.template.cfm"/>
    <cfinclude template="/templates/footer.template.cfm"/>
<cfelse>
    <cflocation url="/rpc/show-issues.cfm" addtoken="false">
</cfif>
