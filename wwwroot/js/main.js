var statusBefore = "";
var needWriteComment = false;

function statusChanged(el) {
    if (statusBefore !== el.value) {
        needWriteComment = true;
    } else {
        needWriteComment = false;
    }
}

function checkInitialIssueStatus() {
    var statusSelector = document.querySelector("#save-issue-status");
    if (statusSelector) {
        var option = document.querySelector("#save-issue-status > option:checked");
        if (option) {
            statusBefore = option.innerText;
        } else {
            statusBefore = "";
        }
    }
}

function validateIssueForm(el) {
    var comment = document.querySelector("#save-issue-comment");
    if (needWriteComment && comment.value === "") {
        comment.style.borderColor = "red";
        return false;
    }
}

function init() {
    checkInitialIssueStatus();
}

document.addEventListener('DOMContentLoaded', init, false);