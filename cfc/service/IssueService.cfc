component {
    public Struct function showIssues() {
        issues = entityLoad("Issue");
        return {
            "issues" = issues
        };
    }

    public Struct function showIssue(Integer id) {
        if (id == 0) {
            issue = entityNew("Issue");
            issue.id = "";
            issue.name = "";
            issue.description = "";
            issue.status = "";
            issue.urgency = "";
            issue.criticality = "";
            issue.comment = "";
            issue.statuses = issue.getStatusForNew();
            issue.urgencies = issue.getAllUrgencies();
            issue.criticalities = issue.getAllCriticalities();
        } else {
            issue = entityLoad("Issue", id, true);
            issue.statuses = issue.getAvailableStatuses();
            issue.urgencies = issue.getAllUrgencies();
            issue.criticalities = issue.getAllCriticalities();
        }
        return {
            "issue" = issue
        };
    }

    public Issue function saveIssue(
        Integer id,
        String name,
        String description,
        String status,
        String urgency,
        String criticality,
        String comment,
        Integer account_id
    ) {
        account = entityLoad("Account", account_id, true);
        issue = entityLoad("Issue", id, true);
        if (id == 0 or isNull(issue)) {
            // if id is not set or no record in database - 
            // we creating a new record
            issue = entityNew("Issue", {});
            issue.created_at = now();
            issue.setAccount(account);
        }
        // check if status changes and we must write logs
        if (
            // if status changes
            isDefined("issue.status")
            and issue.status != status
            // or if is new record
            or not isDefined("issue.status")
            and status == "New"
        ) {
            needWriteLog = true;
        } else {
            needWriteLog = false;
        }
        //
        issue.name = name;
        issue.description = description;
        issue.status = status;
        issue.urgency = urgency;
        issue.criticality = criticality;
        entitySave(issue);
        // issueLog
        if (needWriteLog) {
            issueLog = entityNew("IssueLog", {});
            issueLog.updated_at = now();
            issueLog.action = status;
            issueLog.comment = comment;
            issueLog.setAccount(account);
            issueLog.setIssue(issue);
            entitySave(issueLog);
        }
        return issue;
    }
}
