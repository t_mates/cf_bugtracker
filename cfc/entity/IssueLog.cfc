component persistent="true" table="issue_log" {
    property name="id" column="id" fieldtype="id" generator="increment";
    property name="updated_at" ormtype="timestamp";
    property name="action" ormtype="string";
    property name="comment" ormtype="text";
    // relations
    property name="account" fieldtype="many-to-one" cfc="Account";
    property name="issue" fieldtype="many-to-one" cfc="Issue";
}   