<div id="issues-container">
    <a class="input-button" href="/rpc/show-issue.cfm">New</a>
    <div class="table">
        <div class="table-header-row">
            <div class="table-header-cell">ID</div>
            <div class="table-header-cell">Name</div>
            <div class="table-header-cell">Status</div>
            <div class="table-header-cell">Urgency</div>
            <div class="table-header-cell">Criticality</div>
            <div class="table-header-cell">Created at</div>
        </div>
        <cfoutput>
            <cfloop array="#pageData.issues#" index="issue">
                <a class="table-row" href="/rpc/show-issue.cfm?id=#issue.id#">
                    <div class="table-cell">#issue.id#</div>
                    <div class="table-cell">#issue.name#</div>
                    <div class="table-cell">#issue.status#</div>
                    <div class="table-cell">#issue.urgency#</div>
                    <div class="table-cell">#issue.criticality#</div>
                    <div class="table-cell">#issue.created_at#</div>
                </a>
            </cfloop>
        </cfoutput>
    </div>
</div>
